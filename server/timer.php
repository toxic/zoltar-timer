<?php
header("Access-Control-Allow-Origin: *");

$file = "instances.json";
$method = $_SERVER["REQUEST_METHOD"];
$data = json_decode(file_get_contents("php://input"), true);

$sem = sem_get(207141);
if (sem_acquire($sem)) {
    $instances_json = json_decode(file_get_contents($file), true);

    $isPaused = $instances_json["isPaused"];
    $timeToConquer = $instances_json["timeToConquer"];

    //remove non-active instances:
    $current_time = time();
    $preserve_period = 120; //in seconds
    foreach($instances_json as $index => $instance) {
        $instance_timestamp = $instance["timestamp"];
        if (!$instance_timestamp || $instance_timestamp < $current_time - $preserve_period) {
            //print_r($instance);
            unset($instances_json[$index]);
        }
    }

    $instances_json["isPaused"] = $isPaused;
    $instances_json["timeToConquer"] = $timeToConquer;

//    if ($method == "POST") {
    if (isset($data["requestNewUuid"])) { //to jest glupie, ale nie mam czasu sie bawic w poprawne
                            // POSTY, GETY, CORSY i inne takie tam. Wiec bedzie za pomoca parametru
                            // w GECIE. Trudno.
//    if (empty($uuid)) {
        $uuid = uniqid();
        $fresh_instance = array("redTime" => 0, "blueTime" => 0, "timestamp" => time());
        $instances_json[$uuid] = $fresh_instance;
        print(json_encode(array("uuid" => $uuid, "isPaused" => $isPaused, "timeToConquer" => $timeToConquer)));
    } else if (isset($data["changeIsPaused"])) {
        $instances_json["isPaused"] = $data["changeIsPaused"];
        print(json_encode($instances_json));
    } else if (isset($data["timeToConquer"])) {
        $instances_json["timeToConquer"] = $data["timeToConquer"];
        print(json_encode($instances_json));
    } else if (isset($data["uuid"])){
        $uuid = $data["uuid"];
        $instance = $instances_json[$uuid];
        if (isset($data["redTime"])) {
            $redTime = $data["redTime"];
        } else {
            $redTime = $instance["redTime"];
        }
        $redTime = $redTime ?: 0;
        if (isset($data["blueTime"])) {
            $blueTime = $data["blueTime"];
        } else {
            $blueTime = $instance["blueTime"];
        }
        $blueTime = $blueTime ?: 0;
        if (isset($data["owner"])) {
            $owner = $data["owner"];
        } else {
            $owner = $instance["owner"];
        }
        $owner = $owner ?: "nobody";

        $instance["redTime"] = $redTime;
        $instance["blueTime"] = $blueTime;
        $instance["owner"] = $owner;
        $instance["timestamp"] = $current_time;
        $instances_json[$uuid] = $instance;
        print(json_encode(array("uuid" => $uuid, "isPaused" => $isPaused, "timeToConquer" => $timeToConquer, "redTime" => $redTime, "blueTime" => $blueTime, "owner" => $owner)));
    } else {
        print(json_encode($instances_json));
    }
    file_put_contents($file, json_encode($instances_json));
    sem_release($sem);
} else {
    //Something went wrong...
}

?>
