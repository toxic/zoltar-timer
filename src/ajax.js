ajax = function(url, params) {
    var xhr = cc.loader.getXMLHttpRequest();
    xhr.open(params.method ? params.method : "POST", url);
//  xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xhr.onerror = function (e) {
        if (xhr.hasRunCallbacks) {
            return;
        }
        if (params.error) {
            params.error(xhr)
        }
        if (params.complete) {
            params.complete(xhr);
        }
        xhr.hasRunCallbacks = true;
    };
    xhr.onreadystatechange = function () {
        if (xhr.hasRunCallbacks) {
            return;
        }
        if (xhr.readyState == 4) {
            if (xhr.status >= 200 && xhr.status <= 207) {
                if (params.success) {
                    params.success(JSON.parse(xhr.responseText));
                }
            } else {
                if (params.error) {
                    params.error(xhr);
                }
            }
            if (params.complete) {
                params.complete(xhr);
            }
            xhr.hasRunCallbacks = true;
        }
    }
    if (params.data) {
        var a = xhr.send(JSON.stringify(params.data));
    } else {
        xhr.send();
    }
};
