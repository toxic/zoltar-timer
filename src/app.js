uuid = undefined;

owner = "nobody";

secondsToString = function(totalSec, separator) {
    var minutes = parseInt( totalSec / 60 ) % 60;
    var seconds = parseInt( totalSec % 60 );
    var result = (minutes < 10 ? "0" + minutes : minutes) + separator + (seconds  < 10 ? "0" + seconds : seconds);
    return result;
};

setPaused = function(newValue) {
    isPaused = newValue;
    pausedLabel.setVisible(isPaused);
    pauseButton.setTexture(isPaused ? res.play : res.pause);
    redTimeLabel.setColor(cc.color(255,0,0));
    blueTimeLabel.setColor(cc.color(0,0,255));
    redButton.setOpacity(55);
    blueButton.setOpacity(55);
    redButton.setColor(cc.color(255,255,255));
    blueButton.setColor(cc.color(255,255,255));
    if (!isPaused) {
        if (owner === "red") {
            blueTimeLabel.setColor(cc.color(55,55,55));
            redButton.setColor(cc.color(255,0,0));
            redButton.setOpacity(255);
        } else if (owner === "blue") {
            redTimeLabel.setColor(cc.color(55,55,55));
            blueButton.setColor(cc.color(0,0,255));
            blueButton.setOpacity(255);
        }
    }
}

setOwner = function(newValue) {
    if (newValue === "red") {
        redButton.setOpacity(255);
        redButton.setColor(cc.color(255,0,0));
        redTimeLabel.setColor(cc.color(255,0,0));
        blueButton.setOpacity(55);
        blueButton.setColor(cc.color(255,255,255));
        blueTimeLabel.setColor(cc.color(55,55,55));
        owner = "red";
    } else if (newValue === "blue") {
        blueButton.setOpacity(255);
        blueButton.setColor(cc.color(0,0,255));
        blueTimeLabel.setColor(cc.color(0,0,255));
        redButton.setOpacity(55);
        redButton.setColor(cc.color(255,255,255));
        redTimeLabel.setColor(cc.color(55,55,55));
        owner = "blue";
    } else {
        cc.warn("invalid owner: " + newValue + " should be red|blue.");
    }
}

var serverUrl = "http://students.mimuw.edu.pl/~ps209499/zoltar/timer.php";
var DEFAULT_TIME_TO_CONQUER = 3.0;
var timeToConquer = DEFAULT_TIME_TO_CONQUER;
var hasInitializedWithServer = false;

function initConnection() {
    var data = uuid ? {uuid: uuid} : {requestNewUuid: true};
    ajax(serverUrl, {
        data: data,
        success: function (json) {
            if (!uuid) {
                uuid = json.uuid;
            }
            if (!hasInitializedWithServer) {
                redTime += json.redTime || 0;
                blueTime += json.blueTime || 0;
                if (owner === "nobody" && json.owner && json.owner !== "nobody") {
                    setOwner(json.owner);
                }
                hasInitializedWithServer = true;
            }
            if (json.isPaused !== isPaused && !ignoreNextPauseStateFromServer) {
                setPaused(json.isPaused);
            }
            timeToConquer = json.timeToConquer || DEFAULT_TIME_TO_CONQUER;
            connectionStatus.setTexture(res.connected);
        },
        error: function () {
            connectionStatus.setTexture(res.noConnection);
        },
        complete: function () {
            connectionStatus.stopAction(connectionStatusAction);
            connectionStatus.setRotation(0);
            timeToSync = 1.0;
        }
    });
}

var HelloWorldLayer = cc.Layer.extend({
    sprite:null,
    ctor:function () {
        this._super();
        var size = cc.winSize;

        pressing = undefined;

        var conquerProgress = 0.0;

        pausedLabel = new cc.LabelTTF("Paused", "Arial", 38);
        pausedLabel.setPosition(size.width / 2, size.height - 40);
        pausedLabel.setAnchorPoint(0.5, 0.5);
        this.addChild(pausedLabel, 10);
        pausedLabel.setVisible(false);

        progressBarBackground = new cc.Sprite(res.progressBarBackground);
        progressBarBackground.setPosition((size.width - 274) / 2, size.height - 40);
        progressBarBackground.setAnchorPoint(0.0, 0.5);
        progressBarBackground.setScale(0.75, 1);
        this.addChild(progressBarBackground);
        progressBarBackground.setVisible(false);

        progressBarMeter = new cc.Sprite(res.progressBarMeter);
        progressBarMeter.setPosition((size.width - 274) / 2, size.height - 40);
        progressBarMeter.setAnchorPoint(0.0, 0.5);
        progressBarMeter.setScale(0.75, 1);
        this.addChild(progressBarMeter);
        progressBarMeter.setVisible(false);

        isPaused = false;
        timeToTrigger = 0;
        pauseButton = new cc.Sprite(res.pause);
        pauseButton.setPosition(size.width / 2, 0.68 * size.height);
        pauseButton.setScale(0.5);
        pauseButton.setOpacity(168);
        this.addChild(pauseButton, 2);

        syncIndicator = new cc.Sprite(res.greenLed);
        syncIndicator.setPosition((size.width / 2) + 24, (0.44 * size.height) + 20);
        syncIndicator.setScale(0.25);
        syncIndicator.setOpacity(255);
        this.addChild(syncIndicator, 5);
        syncIndicator.setVisible(false);

        connectionStatus = new cc.Sprite(res.connecting);
        connectionStatus.setPosition(size.width / 2, 0.44 * size.height);
        connectionStatus.setScale(0.33);
        connectionStatus.setOpacity(168);
        this.addChild(connectionStatus, 3);
        connectionStatusAction = cc.repeatForever(cc.rotateBy(1.0, 360));
        connectionStatus.runAction(connectionStatusAction);
        initConnection();

        powerButton = new cc.Sprite(res.power);
        powerButton.setPosition(size.width / 2, 0.20 * size.height);
        powerButton.setScale(0.5);
        powerButton.setOpacity(168);
        this.addChild(powerButton, 2);

        redTimeLabel = new cc.LabelTTF("00:00", "Courier", 58);
        redTimeLabel.setPosition(70, size.height - 20);
        redTimeLabel.setAnchorPoint(0, 1);
        redTimeLabel.setColor(cc.color(255, 0, 0, 0));
        this.addChild(redTimeLabel, 10);

        blueTimeLabel = new cc.LabelTTF("00:00", "Courier", 58);
        blueTimeLabel.setPosition(size.width - 70, size.height - 20);
        blueTimeLabel.setAnchorPoint(1, 1);
        blueTimeLabel.setColor(cc.color(0, 0, 255, 0));
        this.addChild(blueTimeLabel, 10);

        redButton = new cc.Sprite(res.button);
        redButton.setAnchorPoint(0,0);
        redButton.setScale(325/373, (size.height-100)/368);
        redButton.setOpacity(55);
        this.addChild(redButton, 8);

        blueButton = new cc.Sprite(res.button);
        blueButton.setAnchorPoint(1,0);
        blueButton.setPosition(size.width, 0);
        blueButton.flippedX = true;
        blueButton.setScale(325/373, (size.height-100)/368);
        blueButton.setOpacity(55);
        this.addChild(blueButton, 8);

        this.onTouchBegan = function (touch, event) {
            timeToTrigger = 1.0;
            var location = touch.getLocation();
            if (cc.rectContainsPoint(redButton.getBoundingBox(), location) && owner !== "red" && !isPaused) {
                redButton.setOpacity(128);
                redButton.setColor(cc.color(255,128,128));
                pressing = "red";
                timeToTrigger = timeToConquer;
                return true;
            } else if (cc.rectContainsPoint(blueButton.getBoundingBox(), location) && owner !== "blue" && !isPaused) {
                blueButton.setOpacity(128);
                blueButton.setColor(cc.color(128,128,255));
                pressing = "blue";
                timeToTrigger = timeToConquer;
                return true;
            } else if (cc.rectContainsPoint(pauseButton.getBoundingBox(), location)) {
                pauseButton.setOpacity(200);    
                pressing = "pause";
                return true;
            } else if (cc.rectContainsPoint(powerButton.getBoundingBox(), location)) {
                powerButton.setOpacity(200);
                pressing = "power";
                return true;    
            } else if (cc.rectContainsPoint(connectionStatus.getBoundingBox(), location)) {
                connectionStatus.setOpacity(200);
                pressing = "connection";
                return true;    
            } else {
                return false;
            }
        };
        this.onTouchMoved = function (touch, event) {
            var target = event.getCurrentTarget();
            var touchPoint = touch.getLocation();
        };
        this.onTouchEnded = function (touch, event) {
            if (pressing === "red" && owner !== "red") {
                redButton.setOpacity(55);
                redButton.setColor(cc.color(255,255,255));
            } else if (pressing === "blue" && owner !== "blue") {
                blueButton.setOpacity(55);
                blueButton.setColor(cc.color(255,255,255));
            } else if (pressing === "pause") {
                pauseButton.setOpacity(168);
            } else if (pressing === "power") {
                powerButton.setOpacity(168);    
            } else if (pressing === "connection") {
                connectionStatus.setOpacity(168);
            }
            pressing = undefined;
        };

        cc.eventManager.addListener({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: this.onTouchBegan,
            onTouchMoved: this.onTouchMoved,
            onTouchEnded: this.onTouchEnded
        }, this);

        timeToBlink = 1.0;
        timeToSync = 1.0;
        redTime = 0;
        blueTime = 0;
        ignoreNextPauseStateFromServer = false;
        this.scheduleUpdate();
        this.update = function(deltaTime) {
            if (hasInitializedWithServer) {
                if (timeToSync > 0) {
                    timeToSync -= deltaTime;
                    if (timeToSync <= 0) {
                        ajax(serverUrl, {
                            data: {
                                "uuid": uuid,
                                "redTime": Math.floor(redTime),
                                "blueTime": Math.floor(blueTime),
                                "owner": owner
                            },
                            success: function (json) {
                                connectionStatus.setTexture(res.connected);
                                if (json.isPaused !== isPaused && !ignoreNextPauseStateFromServer) {
                                    setPaused(json.isPaused);
                                }
                                timeToConquer = json.timeToConquer || DEFAULT_TIME_TO_CONQUER;
                                syncIndicator.setVisible(true);
                                ignoreNextPauseStateFromServer = false;
                            },
                            error: function() {
                                connectionStatus.setTexture(res.noConnection);
                                syncIndicator.setVisible(false);
                            },
                            complete: function () {
                                timeToSync = 1.0;
                            }
                        });
                    }
                }
            }
            if (pressing === "red" || pressing === "blue") {
                progressBarBackground.setVisible(true);
                progressBarMeter.setVisible(true);
                conquerProgress += deltaTime;
                if (conquerProgress > timeToConquer) {
                    setOwner(pressing);
                    timeToBlink = 0.5;
                    pressing = undefined;
                    conquerProgress = 0;
                    progressBarMeter.setVisible(false);
                    progressBarBackground.setVisible(false);
                }
            } else {
                conquerProgress -= deltaTime;
                if (conquerProgress < 0) {
                    conquerProgress = 0;
                    progressBarMeter.setVisible(false);
                    progressBarBackground.setVisible(false);
                }
            }
            progressBarMeter.setTextureRect(cc.rect(0, 0, Math.round((conquerProgress / timeToConquer) * 366), 17));
            if (timeToTrigger > 0) {
                timeToTrigger -= deltaTime;
                if (timeToTrigger <= 0) {
                    if (pressing === "pause") {
                        setPaused(!isPaused);
                        ignoreNextPauseStateFromServer = true;
                        ajax(serverUrl, {
                            data: {
                                "uuid": uuid,
                                "changeIsPaused": isPaused
                            }
                        });
                    } else if (pressing === "power") {
                        owner = "nobody";
                        redTime = 0;
                        blueTime = 0;
                        redButton.setOpacity(55);
                        redButton.setColor(cc.color(255,255,255));
                        redTimeLabel.setString("00:00");
                        redTimeLabel.setColor(cc.color(255,0,0));
                        blueButton.setOpacity(55);
                        blueButton.setColor(cc.color(255,255,255));
                        blueTimeLabel.setString("00:00");
                        blueTimeLabel.setColor(cc.color(0,0,255));
                    } else if (pressing === "connection") {
                        connectionStatus.setOpacity(168);
                        initConnection();
                    }    
                }
            }
            if (isPaused || owner === "nobody") {
                redTimeLabel.setString(secondsToString(redTime, ":"));
                blueTimeLabel.setString(secondsToString(blueTime, ":"));
                return;
            }
            timeToBlink -= deltaTime;
            while (timeToBlink < 0) {
                timeToBlink += 1.0;
            }
            if (owner === "red") {
                var redTimeBefore = redTime;
                redTime += deltaTime;
                if (parseInt(redTimeBefore) !== parseInt(redTime)) {
                    syncIndicator.setVisible(false);
                }
                redTimeLabel.setString(secondsToString(redTime, (timeToBlink < 0.5 ? ":" : " ")));
                blueTimeLabel.setString(secondsToString(blueTime, ":"));
            } else if (owner === "blue") {
                var blueTimeBefore = blueTime;
                blueTime += deltaTime;
                if (parseInt(blueTimeBefore) !== parseInt(blueTime)) {
                    syncIndicator.setVisible(false);
                }
                blueTimeLabel.setString(secondsToString(blueTime, (timeToBlink < 0.5 ? ":" : " ")));
                redTimeLabel.setString(secondsToString(redTime, ":"));
            }
        }


        return true;
    }
});

var HelloWorldScene = cc.Scene.extend({
    onEnter:function () {
        this._super();
        var layer = new HelloWorldLayer();
        this.addChild(layer);
    }
});

