var res = {
    button : "res/button.png",
    play: "res/play.png",
    pause: "res/pause.png",
    power: "res/power.png",
    progressBarBackground: "res/progress-bar-background.png",
    progressBarMeter: "res/progress-bar-meter.png",
    connecting: "res/connecting.png",
    connected: "res/connected.png",
    noConnection: "res/no-connection.png",
    greenLed: "res/green-led.png"
};

var g_resources = [];
for (var i in res) {
    g_resources.push(res[i]);
}